class Bank():
	crisis = False
	def createAtm(self):
		while not self.crisis:
			yield ("$100")

hsbc = Bank()
cornerStreetAtm = hsbc.createAtm()
print(cornerStreetAtm.next())
hsbc.crisis = True
try:
	print(cornerStreetAtm.next())
except:
	pass
hsbc.crisis = False
print(cornerStreetAtm.next())