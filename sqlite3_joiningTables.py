import sys
import sqlite3

db = sqlite3.connect(":memory:")
cs = db.cursor()

# image items
cs.execute("CREATE TABLE if not exists imageItems(name TEXT, guid TEXT)")
cs.execute("INSERT INTO imageItems(name, guid) VALUES(?,?)", ('smoke', 'i1234567'))
cs.execute("INSERT INTO imageItems(name, guid) VALUES(?,?)", ('smoke', 'i1234599'))

# sequence items
cs.execute("CREATE TABLE if not exists sequenceItems(guid TEXT, frameCount INT, subItems TEXT)")
cs.execute("INSERT INTO sequenceItems(guid, frameCount, subItems) VALUES(?,?,?)", ('i1234568', 1, 'i1234567'))
cs.execute("INSERT INTO sequenceItems(guid, frameCount, subItems) VALUES(?,?,?)", ('i1234569', 1, 'i1234566'))

# source: https://www.w3resource.com/sqlite/sqlite-natural-join.php
# Select all subItems of sequence items
cs.execute('''SELECT subItems FROM sequenceItems''')
# Convert to list of str to use in other query
subItems = [i[0] for i in list(cs.fetchall())]
print("Looking for items: %s"%(subItems))
# Only select image items, that are not part of a sequence
cs.execute('''SELECT guid AS id, name AS nom FROM imageItems WHERE NOT INSTR(?, guid)''', [str(subItems)])
row = cs.fetchall()
