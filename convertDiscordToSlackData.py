#!/usr/bin/python3
import csv
import datetime
import sys
import os
from pathlib import Path

# Read csv file
def convertToSlackRows(inputFilePath, channelName):
    if not channelName:
        print(f"Invalid channelname '{channelName}'")
        return
    newDataRows = [] # ["date", "channel", "username", "text"]
    # READ DATA
    #----------------------------------------
    with open(inputFilePath, mode="r", encoding="utf-8") as csvFile:
        csvReader = csv.reader(csvFile, delimiter=';')
        lineCount = 0
        columnNames = None
        for row in csvReader:
            if lineCount == 0:
                columnNames = row
                print(f'Column names are {", ".join(columnNames)}')
            else:
                # userId = row[0]
                # attachments = row[4]
                # reactions = row[5]
                userName = row[1]
                date = row[2]
                convDate = datetime.datetime.strptime(date, "%d-%b-%y %I:%M %p") # 06-Dec-18 08:51 AM
                unixDate = int(convDate.replace(tzinfo=datetime.timezone.utc).timestamp())
                content = row[3]
                newRow = [unixDate, channelName, userName, content]
                print(f'{newRow}')
                newDataRows.append(newRow)
            lineCount += 1
    return newDataRows

def main(dirPath):
    # CONVERT ALL CHANNEL DATA TO SLACK CSV ROWS
    #----------------------------------------
    allChannelRows = []
    for fileName in os.listdir(dirPath):
        filePath = Path(dirPath, fileName)
        if filePath.is_file():
            channelName = fileName.split("_")[1]
            slackRows = convertToSlackRows(filePath, channelName)
            allChannelRows.extend(slackRows)
    # WRITE DATA
    #----------------------------------------
    newFileName = f"kisamo_server_slack.csv"
    outputFilePath = Path(dirPath) / "converted" / newFileName
    if not outputFilePath.parent.exists():
        print("Creating folder...")
        outputFilePath.parent.mkdir()
    with open(outputFilePath, mode="w", encoding="utf-8", newline='') as outputFile:
        print(f"Writing data to {outputFilePath}")
        csvWriter = csv.writer(outputFile, delimiter=",", quotechar='"', quoting=csv.QUOTE_ALL)
        for row in allChannelRows:
            csvWriter.writerow(row)
    print('Done.')

def cleanUpFileNames(dirPath):
    for entryName in os.listdir(dirPath):
        oldPath = Path(dirPath) / entryName
        newPath = Path(dirPath) / entryName.replace("-", "").replace(" ", "_").replace("__", "_")
        os.rename(oldPath, newPath)

if __name__ == "__main__":
    # DIRPATH = Path("C:/Users/fynn/Desktop/kisamo/discord_data/")
    DIRPATH = sys.argv[1]
    print(DIRPATH)
    cleanUpFileNames(DIRPATH)
    main(DIRPATH)
