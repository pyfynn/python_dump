import queue
import time
import threading

q = queue.Queue()
threads = []

class Task(threading.Thread):
    def __init__(self, queue):
        super().__init__()
        self.queue = queue

    def run(self):
        while True:
            args = self.queue.get()
            if args:
                time.sleep(0.5)
                print(args)
                self.queue.task_done()

newTask = Task(q)
newTask.setDaemon(True)
newTask.start()
q.put("Doing thready things...") # this will run
q.join()
q.put("Doing more thready things...") # this won't run because main thread finishes before task is done
print('All Done.')
