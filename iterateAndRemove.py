lst = [1,2,3,4,5,6,5,4,3,2,1]

for index, val in reversed(list(enumerate(lst))):
    if val % 2 == 0:
        lst[:] = [x for x in lst if x is not val]

# Result should be 1,3,5,5,3,1
print lst
