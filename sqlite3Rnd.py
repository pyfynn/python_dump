import sys
import json
import sqlite3

db = sqlite3.connect(":memory:", detect_types=sqlite3.PARSE_DECLTYPES) # detect_types has to be defined to apply conversion when selecting special columns
cursor = db.cursor()

cursor.execute("CREATE TABLE if not exists items(guid TEXT, tags JSON)")

item1 = {
    "guid" : "i1234567",
    "tags" : ['t1234567', 't1234568']
}

item2 = {
    "guid" : "i1234568",
    "tags" : ['t1234569', 't1234570']
}

items = [item1, item2]

# Enable inserting lists
# source: https://stackoverflow.com/questions/48467144/insert-list-into-sqlite3-cell
def adaptListToText(inputList):
    return ",".join(inputList)

def convertTextToList(inputText):
    return inputText.split(",")
sqlite3.register_adapter(list, adaptListToText)
sqlite3.register_converter('JSON', convertTextToList)

# Insert item1
# cursor.execute('''INSERT INTO items(guid, tags) VALUES(?,?)''', (item1['guid'],item1['tags']))

# Insert all items
itemsValues = [
    [item1['guid'], item1['tags']],
    [item2['guid'], item2['tags']]
]
cursor.executemany('''INSERT INTO items(guid, tags) VALUES(?,?)''', (itemsValues))

cursor.execute('''SELECT tags AS "tags [JSON]" FROM items''')
print(cursor.fetchall())
sys.exit(1)

# Search matches
#------------------------------
# Match single value argugment
cursor.execute("SELECT * FROM items WHERE guid = ?", (item1['guid'],))
# print(cursor.fetchall())

# Use text search on tags
# source: http://www.sqlitetutorial.net/sqlite-functions/sqlite-instr/
cursor.execute("SELECT * FROM items WHERE INSTR(tags, ?)", (item1['tags'][0], ))
# print(cursor.fetchall())

# Search in multiple columns (with named placeholders to use same search arg)
# Use string substitution to match some or all arguments
cursor.execute("""SELECT * FROM items
            WHERE INSTR(tags, :searchArg1)
            {0}
            INSTR(guid, :searchArg1)""".format("OR"), ({'searchArg1' : 'i1234567'}))

# Reg exp search
# You need to define your own function to execute the regexp search
import re
def matchRegExp(expr, item):
    return re.match(expr, item) is not None
db.create_function("MATCHREGEX", 2, matchRegExp) # syntax: https://docs.python.org/3/library/sqlite3.html#sqlite3.Connection.create_function
cursor = db.cursor()
cursor.execute("SELECT * FROM items WHERE MATCHREGEX('i[0-9]', guid);")

cursor.execute("""SELECT guid FROM items WHERE guid=?""", ("i1234567",))
# print(cursor.fetchall())
