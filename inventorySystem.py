class Item(object):
    def __init__(self, name, description):
        self.name = name
        self.description = description

class Inventory(object):
    def __init__(self):
        self.items = {}

    def addItem(self, name, description):
        newItem = Item(name, description)
        self.items[name] = newItem

    def getItem(self, name):
        return self.items[name]

def main():
    inventory = Inventory()
    inventory.addItem("pickaxe", "For axing stuff")
    print inventory.getItem("pickaxe").description

if __name__ == "__main__":
    main()
