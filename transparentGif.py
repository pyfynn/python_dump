import os
from PySide2 import QtCore, QtGui, QtWidgets

class LoadingAnimationWidget(QtWidgets.QFrame):
    def __init__(self, alpha=0.5, parent=None):
        super(LoadingAnimationWidget, self).__init__(parent)
        # UI
        filePath = os.path.join(os.path.dirname(__file__),"transparentGif_dottedGlowyCircle.gif")
        filePath = os.path.join(os.path.dirname(__file__),"transparentGif_disintegratingTriangle.gif")
        filePath = os.path.join(os.path.dirname(__file__),"transparentGif_scienceBubblesSquare.gif")
        filePath = os.path.join(os.path.dirname(__file__),"transparentGif_hBarsCropped.gif")
        # filePath = os.path.join(os.path.dirname(__file__),"transparentGif_beachBallAlpha.gif")
        layout = QtWidgets.QHBoxLayout(self)
        # textLabel
        self.textLabel = QtWidgets.QLabel("Loading...")
        self.textLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.textLabel.setFixedSize(250, 250)
        layout.addWidget(self.textLabel)
        # animLabel
        self.animLabel = QtWidgets.QLabel()
        self.animLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.animLabel.setScaledContents(True)
        self.animLabel.setFixedSize(250, 250)
        layout.addWidget(self.animLabel)
        # movie
        self.movie = None
        # INIT
        if os.path.exists(filePath):
            # Emulate background color from image corner pixel
            image = QtGui.QImage(filePath)
            color = image.pixelColor(0, 0)
            self.setStyleSheet("background-color: rgb%s;"%str(color.getRgb()))
            # Set movie
            self.movie = QtGui.QMovie(filePath, QtCore.QByteArray())
            self.movie.setCacheMode(QtGui.QMovie.CacheAll)
            self.movie.setSpeed(100)
            self.movie.start()
            self.animLabel.setMovie(self.movie)
            self.animLabel.setHidden(False)
            self.textLabel.setHidden(True)
        else:
            self.setStyleSheet("background: rgb(0, 0, 0, 255);")
            self.animLabel.setHidden(True)
            self.textLabel.setHidden(False)
        # NOTE: Opacity effect needs to be accessible by each instance, so you HAVE to put it in self.opacityEffect
        # Alternatively you can set the opacity on each instance manually from the base scobe after it has been istanciated. This sucks though. So don't do that, mkay.
        self.opacityEffect = QtWidgets.QGraphicsOpacityEffect()
        self.opacityEffect.setOpacity(alpha)
        self.setGraphicsEffect(self.opacityEffect)

class MainWindow(QtWidgets.QDialog):
    animationWidget = None
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        layout = QtWidgets.QVBoxLayout(self)
        self.textLabel = QtWidgets.QLabel("something something something\nsomething something something\nsomething something something")
        self.textLabel.setAlignment(QtCore.Qt.AlignCenter)
        layout.addWidget(self.textLabel)
        self.animationWidget = LoadingAnimationWidget(parent=self)
        # INIT
        self.resize(500, 500)

    def resizeEvent(self, event):
        self.animationWidget.setFixedSize(self.size())

def main():
    mWin = MainWindow()
    mWin.exec_()

if __name__ == "__main__":
    main()
