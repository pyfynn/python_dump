"""
file: audioToMp3.py
info: Converts input audio file to mp3 using ffmpeg
"""
import os
import argparse
import subprocess


def main(args):
    """
    Converts input audio file to mp3 using ffmpeg
    """
    # VARIABLES
    inputs = args.inputs
    outputDir = args.output
    channels = args.channels
    sampleRate = args.samplerate
    bitRate = args.bitrate
    # CHECKS
    for inputPath in inputs:
        print(f"PROCESSING '{inputPath}'")
        if inputPath is None or os.path.exists(inputPath) is False:
            print(f"File does not exist '{inputPath}'")
        if outputDir == "":
            outputDir = os.path.dirname(inputPath)
        if os.path.exists(outputDir) is False:
            print(f"Output directory does not exist '{outputDir}'")
        elif os.path.isfile(outputDir):
            outputDir = os.path.dirname(outputDir)

        # Output file name
        fileName = os.path.basename(os.path.splitext(inputPath)[0])
        outputPath = os.path.join(outputDir, fileName)
        count = 1
        while True:
            if os.path.exists("%s.mp3"%outputPath):
                fileName = "%s_%s"%(fileName, count)
                outputPath = os.path.join(outputDir, fileName)
                count += 1
            else:
                break

        # Command
        # ffmpeg -i input.wav -vn -ar 44100 -ac 2 -ab 192k -f mp3 output.mp3
        cmd = 'ffmpeg -hide_banner -i "{0}" -vn -ar {1} -ac {2} -ab {3}k -f mp3 "{4}.mp3"'.format(inputPath, sampleRate, channels, bitRate, outputPath)
        print(cmd)
        exe = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        print(exe.stdout.read())
    print("Done.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="lol")
    parser.add_argument(dest="inputs",    type=str, nargs="+", help='path to input file')
    parser.add_argument("-o", dest="output",    type=str, nargs="?", default="", help="path to output directory. If empty, placed next to input file")
    parser.add_argument("-c", dest="channels",  type=int, nargs="?", default=2, help="number of audio channels")
    parser.add_argument("-s", dest="samplerate", type=int, nargs="?", default=44100, help="output samplerate")
    parser.add_argument("-b", dest="bitrate",   type=int, nargs="?", default=192, help="output samplerate")
    
    args = parser.parse_args()
    main(args)
