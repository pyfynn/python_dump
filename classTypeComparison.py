class Person:
    def __init__(self):
        self.name = None

class Robert(Person):
    def __init__(self):
        Person.__init__(self)

if __name__ == "__main__":
    person = Person()
    robert = Robert()
    print("%s is instance of %s? %s"%(person, Person, isinstance(person, Person)))
    print("%s is instance of %s? %s"%(robert, Person, isinstance(robert, Person)))
    print("%s is of type %s? %s"%(robert, Person, type(robert) is Person))
