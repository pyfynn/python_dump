import sqlite3

db = sqlite3.connect(":memory:")
cs = db.cursor()

cs.execute("CREATE TABLE if not exists users(id INT PRIMARY KEY, name TEXT)")
cs.execute("INSERT INTO users(name) VALUES(?)", ('fynn',))
cs.execute("SELECT * FROM users")
print(cs.fetchall())
