"""Example for a cache implementation using decorators."""


# Import built-in modules
import functools
import logging
import time

# Initializing our logger
logging.basicConfig()
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)

def cached(function):
    """Use cached value or calculate and store the result in the cache.
    Args:
        function (function): Function to wrap.
    Returns:
        function: Wrapped caching function.
    """
    # The cache
    kwd_mark = object() # sentinel for separating args from kwargs
    cache = {}

    # The wrapper makes a new key in the cache using the function definition and the given arguments.\
    # It stores the result of running the function with those arguments as the value of that key.
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        """Wrap executing the cache.
        Args:
            args (multiple): Arguments to use for calculation.
        Returns:
            function: Wrapped function to execute.
        """
        # signature = (function, args, (kwargs))
        signature = (function, args, tuple(sorted(kwargs.items())))
        LOG.debug("Using signature: %s", signature)
        if signature in cache:
            LOG.debug("Retrieving from cache: %s", signature)
            result = cache[signature]
        else:
            LOG.debug("Calculating: %s", signature)
            result = function(*args, **kwargs)
            LOG.debug("Caching: %s", signature)
            cache[signature] = result
        return result
    return wrapper

# Use the @cached decorator to make a function, that takes a long time to run, cachable
# or @functools.lru_cache() in Python3.x
@cached
def add(number_a, number_b):
    time.sleep(2)
    return number_a + number_b

# Also works for class methods
class Calculator:
    @cached
    def add(self, number_a, number_b, multResult=None):
        result = add(number_a, number_b)
        if multResult:
            result *= multResult
        return result

# INIT
#----------------------------------------
if __name__ == "__main__":
    LOG.setLevel(logging.INFO)
    # Compare first execution to retrieving cached result
    LOG.info("First run...")
    startTime = time.time()
    print(add(1, 1))
    LOG.info("...finished in %s seconds."%(time.time()-startTime))
    LOG.info("Second run with same args...")
    startTime = time.time()
    print(add(1, 1))
    LOG.info("...finished in %s seconds."%(time.time()-startTime))
    # Class method
    calc = Calculator()
    LOG.info("First class run...")
    startTime = time.time()
    print(calc.add(1, 1, multResult=2))
    LOG.info("...finished in %s seconds."%(time.time()-startTime))
    LOG.info("Second class run with same args...")
    startTime = time.time()
    print(calc.add(1, 1, multResult=2))
    LOG.info("...finished in %s seconds."%(time.time()-startTime))
