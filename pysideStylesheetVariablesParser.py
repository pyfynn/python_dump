"""
file: pysideStylesheetVariablesParser.py
info: Parse custom variables and replace their occurences throughout the stylesheet
"""
import os
import re
from PySide import QtGui
BASEDIR = os.path.dirname(__file__)
STYLEFILENAME = "pysideStylesheetWithVariables.qss"
STYLEFILE = os.path.join(BASEDIR, STYLEFILENAME)

def parseStylesheet(rawStylesheet, varID="@"):
    '''
    Replace variable occurences throughout the stylesheet, then apply the stylesheet.
    Variables must be defined at the top of the file.
    : rawStylesheet : string
    : varID : what symbol is used to identify variables
    '''
    lines = rawStylesheet.split("\n")
    ret = ""
    varDefRX = r"\s?(%s[a-zA-Z0-9_]+)(\s=\s)(.+)"%(varID) # group1 = variable definition, group2 = operator (=), group3 = variable value
    varReplRX = r"%s[a-zA-Z0-9_]+"%(varID)
    varValues = {} # {"variableName" : "variableValue",}
    for index, line in enumerate(lines):
        if re.search(varDefRX, line) is not None:
            match = re.search(varDefRX, line)
            varValues[match.group(1)] = match.group(3)
        else:
            parseLine = line
            varNames = re.findall(varReplRX, parseLine)
            for i in varNames:
                try:
                    parseLine = re.sub(i, varValues[i], parseLine)
                except KeyError as err:
                    print "KeyError: Variable %s not defined at line %s of the rawStylesheet"%(err, index)
            ret += "%s\n"%(parseLine)
    return ret

def pysideStylesheetVariablesParser():
    '''
    Examples on how to use the parseStylesheet function
    '''
    # FILE EXAMPLE
    tempFile = open(STYLEFILE, "r")
    rawStylesheet = tempFile.read()
    tempFile.close()
    #parsedStylesheet = parseStylesheet(rawStylesheet)

    # STRING EXAMPLE
    styleSheetString =\
    """
    @darkGreen = rgb(0, 123, 123)
    @lineWidth = 10px
    QWidget{
        background-color: @darkGreen, @lineWidth;
        border: @lineHeight;
    }
    """
    parsedStylesheet = parseStylesheet(styleSheetString)
    return parsedStylesheet

if __name__ == '__main__':
    print pysideStylesheetVariablesParser()
