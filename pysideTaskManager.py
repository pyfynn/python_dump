from PySide2 import QtCore, QtWidgets
import sys
import queue
import time

class Task(QtCore.QThread):
    finished = QtCore.Signal(object, object)

    def __init__(self, function, args, kwargs):
        super().__init__()
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def run(self):
        exe = self.function(*self.args, **self.kwargs)
        self.finished.emit(self, exe)

class TaskManager(QtCore.QThread):
    allDone = QtCore.Signal()
    taskDone = QtCore.Signal(object) # output
    paused = QtCore.Signal()

    def __init__(self, maxRunningTasks=10):
        super().__init__()
        self.queue = queue.Queue()
        self.runningTasks = []
        self._stop = False
        self.maxRunningTasks = maxRunningTasks

    def addTask(self, function, *args, **kwargs):
        newTask = Task(function, args, kwargs)
        newTask.finished.connect(self.onTaskDone)
        self.queue.put((newTask, ))

    def onTaskDone(self, task, taskResult):
        self.taskDone.emit(taskResult)
        self.queue.task_done()
        del self.runningTasks[0]
        # Emit allDone when last task finishes
        if self.queue.empty() and len(self.runningTasks) == 0:
            self.allDone.emit()

    def stop(self):
        self._stop = True

    def run(self):
        while True:
            # Sleep thread for a fraction of a time, so if nothing is happening, the other thread(s) can execute meanwhile (https://stackoverflow.com/questions/54689749/pyside2-gui-slow-while-using-qthreads-movetothread)
            QtCore.QThread.msleep(0) # same as time.sleep(0)
            # Stop Task Manager if everything is done
            if self.queue.empty():
                # Keep event loop running even though all added tasks are done. That way we don't have to start the taskManager again to resume with newly added tasks.
                continue
            # Stop Task manager if stop is pressed
            if self._stop:
                self.paused.emit()
                return
            # Add function to queue
            if len(self.runningTasks) < self.maxRunningTasks:
                args = self.queue.get()
                if args:
                    task = args[0]
                    self.runningTasks.append(task)
                    task.start()

class AppWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(AppWindow, self).__init__()
        # VARIABLES
        self.taskManager = TaskManager()
        # UI
        mainWidget = QtWidgets.QWidget()
        self.setCentralWidget(mainWidget)
        mainLayout = QtWidgets.QVBoxLayout()
        mainWidget.setLayout(mainLayout)  
        startButton = QtWidgets.QPushButton('Start')
        pauseButton = QtWidgets.QPushButton("Pause")
        mainLayout.addWidget(startButton)
        mainLayout.addWidget(pauseButton)
        # EVENTS
        startButton.clicked.connect(self.process)
        pauseButton.clicked.connect(self.cancel)
        self.taskManager.allDone.connect(self.onAllDone)
        self.taskManager.taskDone.connect(self.onTaskDone)
        self.taskManager.paused.connect(self.onPaused)
        # INIT
        self.addTasks()

    def onTaskDone(self, output):
        print("Output received: %s"%output)

    def onAllDone(self):
        print("All Done")
        self.addTasks()

    def onPaused(self):
        print("Paused. %s tasks left"%(self.taskManager.queue.unfinished_tasks))

    def addTasks(self):
        for i in range(50):
            self.taskManager.addTask(mathStuff, i+1, multiplier=2)

    def process(self):
        self.taskManager._stop = False
        self.taskManager.start()
        print('Doing stuff in main thread.')

    def cancel(self):
        self.taskManager.stop()

def mathStuff(base, multiplier=0):
    time.sleep(0.5)
    return base*multiplier

app = QtWidgets.QApplication([])
win = AppWindow()
win.show()
sys.exit(app.exec_())
