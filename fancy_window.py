"""
file: fancy_window.py
info: Simple window to show loading from .ui with icons and background processes
"""
import os
import sys
import time
from PySide import QtCore, QtGui, QtUiTools

baseName = os.path.splitext(__file__)[0]
baseDir = os.path.dirname(__file__)

APP = QtGui.QApplication(sys.argv)
LOADER = QtUiTools.QUiLoader()
GUI = LOADER.load("%s.ui"%baseName)
# Include the resource file. Remember to convert the .qrc file to .rcc : pyside-rcc resource.qrc -o resource.rcc)
execfile("%s.rcc"%(baseName))

class BackgroundUpdater(QtCore.QThread):
    # To run the code inside a new thread, this is a subclass of QThread, which inherits QObject
    progressUpdated = QtCore.Signal(object)
    progressStart = QtCore.Signal()
    progressDone = QtCore.Signal()

    def __init__(self, method=None):
        # Must init QObject else runtime error: PySide.QtCore.Signal object has no attribute 'connect'
        super(BackgroundUpdater, self).__init__() # or : QtCore.QThread.__init__(self)

    def run(self):
        if self.method is None:
            self.progressDone.emit()
        else:
            self.method()

    def lengthyOperation(self):
        # You could define the worker function here...
        self.progressStart.emit()
        numItems = 100
        for index, i in enumerate(range(numItems)):
            time.sleep(.02)
            self.progressUpdated.emit([index, numItems, i])
        self.progressDone.emit()

class MainWindow(QtGui.QDialog):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        # GUI
        self.ui = GUI
        self.mainLayout = QtGui.QVBoxLayout()
        self.mainLayout.setContentsMargins(0, 0, 0, 0)
        self.mainLayout.addWidget(self.ui)
        self.setLayout(self.mainLayout)

        # EVENTS
        self.bgUpdater = BackgroundUpdater()
        self.bgUpdater.method = self.lengthyOperation
        self.ui.startButton.clicked.connect(self.bgUpdater.start)
        self.bgUpdater.progressUpdated.connect(self.updateProgress)
        self.bgUpdater.progressDone.connect(self.resetProgress)

    def gogogo(self):
        # BGTHREAD.start()
        self.bgUpdater.run()

    def lengthyOperation(self):
        # ...Or you could define the worker function here for easier injection to the background updater
        numItems = 100
        for index, i in enumerate(range(numItems)):
            time.sleep(.1)
            self.bgUpdater.progressUpdated.emit([index, numItems, i])
        self.bgUpdater.progressDone.emit()

    def startProgress(self):
        self.ui.progressInfoLabel("updating...")

    def updateProgress(self, data):
        curIndex = data[0]
        numItems = data[1]
        curObject = data[2]
        percent = float(curIndex)/float(numItems)*100.0
        self.ui.progressBar.setValue(percent)
        self.ui.progressInfoLabel.setText("updating %s/%s"%(curObject, numItems))

    def resetProgress(self):
        self.ui.progressBar.setValue(0)
        self.ui.progressInfoLabel.setText("Done.")

if __name__ == '__main__':
    mWin = MainWindow()
    mWin.show()
    sys.exit(APP.exec_())
