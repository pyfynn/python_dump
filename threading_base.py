"""
file: threading_base.py
info: For easily multithreading lengthy operations Based upon from: https://gist.github.com/amirasaran/e91c7253c03518b8f7b7955df0e954bb
"""
import time
import threading

class BaseThread(threading.Thread):
    def __init__(self, callback=None, callback_args=None, *args, **kwargs):
        target = kwargs.pop('target')
        super(BaseThread, self).__init__(target=self.target_with_callback, *args, **kwargs)
        self.callback = callback
        self.method = target
        self.callback_args = callback_args

    def target_with_callback(self):
        # This is run in a new thread
        ret = self.method()
        if self.callback is not None:
            # Pass the result of the method, and additional arguments into the callback method
            self.callback(ret, self.callback_args)

# Example using BaseThread with callback
#==========================================
def my_thread_job():
    # do any things here
    print "1"
    time.sleep(1)
    print "3"

def cb(param1, param2):
    # This is run in the thread that started the job, after my_thread_job is done
    print "4"
    print "{} {}".format(param1, param2)

thread = BaseThread(name='test', target=my_thread_job, callback=cb, callback_args=("hello", "world"))

thread.start()
print "2"