from PySide2 import QtCore, QtWidgets
import sys
import queue
import time

class BackgroundTask(QtCore.QThread):
    finished = QtCore.Signal(list)

    def __init__(self, queue):
        super().__init__()
        self.queue = queue
        self.function = None
        self.args = None
        self.kwargs = None

    def run(self):
        while True:
            queueArgs = self.queue.get()
            if queueArgs is None:
                print("Exiting %s"%(self))
                return
            # Execute function with arguments
            self.function = queueArgs[0]
            self.args = queueArgs[1]
            self.kwargs = queueArgs[2]
            # Run function
            exe = self.function(self.args, self.kwargs)
            self.finished.emit([self, exe])

class BgTaskManager(QtCore.QObject):
    # FIXME : Hacky implementation for rendering stuff in the background. Any function can be passed in. Probably better if this would be more explicit.
    taskDone = QtCore.Signal(object)
    allDone = QtCore.Signal(object)

    def __init__(self):
        super().__init__()
        self.queue = queue.Queue()
        self.tasks = []

    def process(self, function, *args, **kwargs):
        newTask = BackgroundTask(self.queue)
        newTask.finished.connect(self.checkOutput)

        self.tasks.append(newTask)

        newTask.start()
        self.queue.put((function, args, kwargs))
        self.queue.put(None) # To tell task to stop

    def checkOutput(self, output):
        task = output[0]
        result = output[1]
        self.taskDone.emit(result)
        if self.queue.empty():
            self.allDone.emit(True)

def doSomething(arg1, optArg=0):
    time.sleep(1)
    stuff = [arg1, optArg]
    print("Doing something: %s, %s"%(arg1, optArg))
    return stuff

def whenOneDone(inp):
    print("Done: %s"%inp)

def whenAllDone():
    print("All done.")
    sys.exit(1)

app = QtWidgets.QApplication([])
win = QtWidgets.QDialog()

bgWorker = BgTaskManager()
bgWorker.allDone.connect(whenAllDone)
bgWorker.taskDone.connect(whenOneDone)

bgWorker.process(doSomething, "wat", optArg=2)
print('main thread is doing something else...')
bgWorker.process(doSomething, "hah", optArg=5)

sys.exit(app.exec_())
