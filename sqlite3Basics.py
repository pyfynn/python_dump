'''
This file is for learning and testing stuff with sqlite.
sources:
- https://www.pythoncentral.io/introduction-to-sqlite-in-python/
'''
from pathlib import Path
import sqlite3

#==============================
# Opening database for working
#==============================
# Create database in RAM with ':memory:' argument:
db = sqlite3.connect(":memory:")
# Create database on disk with filePath:
dbFilePath = Path(__file__).parent / "sqlite3Test.db"
db = sqlite3.connect(str(dbFilePath.resolve()))
# Close database when done working with it
db.close()

#==============================
# Creating and deleting tables
#==============================
db = sqlite3.connect(str(dbFilePath.resolve()))
# Make cursor object (all operations have to be passed to it)
cursor = db.cursor()
# Check if table users does not exist and create it ...
# ...create a 'users' table with keys: 'id', 'name', 'phone', 'email', 'password'
cursor.execute('''
    CREATE TABLE if not exists users(id INTEGER PRIMARY KEY, name TEXT,
                       phone TEXT, email TEXT unique, password TEXT)
''')
# Write changes to database
# If you close the connection using close or the connection to the file is lost (maybe the program finishes unexpectedly), not committed changes will be lost.
db.commit()
# Delete (drop) a table
cursor.execute('''DROP TABLE users''')
db.commit()
db.close()

#==============================
# Inserting data into a table
#==============================
db = sqlite3.connect(str(dbFilePath.resolve()))
cursor = db.cursor()
cursor.execute('''
    CREATE TABLE if not exists users(id INTEGER PRIMARY KEY, name TEXT,
                       phone TEXT, email TEXT unique, password TEXT)
''')
# Insert data using tuple
#------------------------------
# user1
name1 = "Andres"
phone1 = "3366858"
email1 = "user@example.com"
password1 = "12345"
# Insert user1
cursor.execute('''INSERT INTO users(name, phone, email, password)
                  VALUES(?,?,?,?)''', (name1, phone1, email1, password1))
print('First user inserted')
# Inser data using dict
#------------------------------
user2 = {
    "name" : 'John',
    "phone" : '5557241',
    "email" : 'johndoe@example.com',
    "password" : 'abcdef'
}
cursor.execute('''INSERT INTO users(name, phone, email, password)
                    VALUES(:name, :phone, :email, :password)''', user2)
print('Second user inserted')
# Insert multiple users at once
#------------------------------
# Delete previous entries first to avoid 'UNIQUE' error
cursor.execute('''DROP TABLE users''')
cursor.execute('''
    CREATE TABLE users(id INTEGER PRIMARY KEY, name TEXT,
                       phone TEXT, email TEXT unique, password TEXT)
''')
users = [
    [name1, phone1, email1, password1],
    [user2['name'], user2['phone'], user2['email'], user2['password']]
]
cursor.executemany('''INSERT INTO users(name, phone, email, password) VALUES(?,?,?,?)''', users)
# Show id of row in the table that the data was inserted
print('Inserted user1 and user2 at row %s'%(cursor.lastrowid))
db.commit()
db.close()

#==============================
# Retrieving (select) data from a table
#==============================
db = sqlite3.connect(str(dbFilePath.resolve()))
cursor = db.cursor()
# Make a query for a row to be selected
cursor.execute('''SELECT name, email, phone FROM users''')
# Get the first match
user1 = cursor.fetchone()
# Print the first column retrieved user's name
print(user1[0])
# Get all matches
all_rows = cursor.fetchall()
for row in all_rows:
    # The order of the tuple is defined in the SELECT command above
    print('{0} : {1}, {2}'.format(row[0], row[1], row[2]))
# Or iterate over the cursor (acts as fetchall) after selecting
cursor.execute('''SELECT name, email, phone FROM users''')
for row in cursor:
    print('{0} : {1}, {2}'.format(row[0], row[1], row[2]))
# To retrive data with conditions, use again the "?" placeholder:
user_id = 2
cursor.execute('''SELECT name, email, phone FROM users WHERE id=?''', (user_id,))
print(cursor.fetchone())
db.close()
# Access selected data using key names instead of index by setting row_factory
#------------------------------
db = sqlite3.connect(str(dbFilePath.resolve()))
db.row_factory = sqlite3.Row
cursor = db.cursor()
cursor.execute('''SELECT name, email, phone FROM users''')
for row in cursor:
    print('{0} : {1}, {2}'.format(row['name'], row['email'], row['phone']))
db.close()

#==============================
# Updating and deleting data
#==============================
db = sqlite3.connect(str(dbFilePath.resolve()))
cursor = db.cursor()
# Update user with id 1
newphone = '31130931'
userid = 1
print("Updating user %s's phone to %s"%(userid, newphone))
cursor.execute('''UPDATE users SET phone = ? WHERE id = ? ''', (newphone, userid))
cursor.execute('''SELECT phone FROM users''')
print(cursor.fetchall())
# Delete user with id 2
delete_userid = 2
cursor.execute('''DELETE FROM users WHERE id = ? ''', (delete_userid, ))
# Discard any changes made since last commit
# db.rollback()
db.commit()
db.close()

#==============================
# Error handling
#==============================
# For best practices always surround the database operations with a try clause or a context manager:
try:
    db = sqlite3.connect(str(dbFilePath.resolve()))
    cursor = db.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS
                      users(id INTEGER PRIMARY KEY, name TEXT, phone TEXT, email TEXT unique, password TEXT)''')
    # Commit the change
    db.commit()
# Catch the exception (usually you would make a catch for each exception type)
# More on exceptions:  https://www.pythoncentral.io/catching-python-exceptions-the-try-except-else-keywords/
except Exception as e:
    # Roll back any change if something goes wrong
    db.rollback()
    raise e
finally:
    # Close the db connection
    db.close()
