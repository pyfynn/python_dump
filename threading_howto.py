import time
from threading import Thread

class Manager(object):
    def new_thread(self):
        return _Thread(parent=self)

    def on_thread_finished(self, thread, data):
        # This is run in the main thread
        print thread, data
        pass

class _Thread(Thread):
    def __init__(self, parent=None):
        self.parent = parent
        super(_Thread, self).__init__()

    def run(self):
        # This is run in a new thread
        time.sleep(1)
        self.parent.on_thread_finished(self, 42)

mgr = Manager()
thread = mgr.new_thread()
thread.start()